'use strict';
const AWS = require('aws-sdk');
const ec2 = new AWS.EC2();


// ###############################################################
// #                       GET
// ###############################################################

module.exports.getInstances = async (event) => {
  const name = event.pathParameters.name || '';

  const params = {
    Filters: [
      {
        Name: 'tag:Name',
        Values: [`*${name}*`],
      },
    ],
  };

  try {
    const data = await ec2.describeInstances(params).promise();
    const instances = data.Reservations.flatMap((reservation) =>
      reservation.Instances.map((instance) => ({
        id: instance.InstanceId,
        name: instance.Tags.find((tag) => tag.Key === 'Name').Value,
        size: instance.InstanceType,
      }))
    );

    return {
      statusCode: 200,
      body: JSON.stringify({ instances }),
    };
  } catch (error) {
    return {
      statusCode: 500,
      body: JSON.stringify({ error: 'An error occurred while retrieving instances' }),
    };
  }
};





// ###############################################################
// #                       PUT
// ###############################################################

module.exports.updateInstance = async (event) => {
  try {
    const { name } = JSON.parse(event.body);

    // Validate if name is provided
    if (!name) {
      return {
        statusCode: 400,
        body: JSON.stringify({ error: 'Name is required for updating the instance' }),
      };
    }

    // Update the instance name
    await updateEC2InstanceName(event.pathParameters.instanceId, name);

    return {
      statusCode: 200,
      body: JSON.stringify({ message: 'Instance name updated successfully' }),
    };
  } catch (error) {
    console.error('Error:', error);
    return {
      statusCode: 500,
      body: JSON.stringify({ error: 'An error occurred' }),
    };
  }
};

const updateEC2InstanceName = async (instanceId, name) => {
  try {
    const tagParams = {
      Resources: [instanceId],
      Tags: [
        { Key: 'Name', Value: name },
        ,
      ],
    };

    // Update the instance tags
    await ec2.createTags(tagParams).promise();
  } catch (error) {
    console.error('Error updating EC2 instance name:', error);
    throw error;
  }
};


// ###############################################################
// #                       DELETE
// ###############################################################


module.exports.deleteInstance = async (event) => {
  const instanceIdd = event.pathParameters.instanceIdd;

  try {
    await ec2.terminateInstances({ InstanceIds: [instanceIdd] }).promise();

    return {
      statusCode: 200,
      body: JSON.stringify({ message: 'Instance deleted successfully' }),
    };
  } catch (error) {
    return {
      statusCode: 500,
      body: JSON.stringify({ error: 'An error occurred while deleting the instance' }),
    };
  }
};

// ###############################################################
// #                       CREATE
// ###############################################################

module.exports.createInstance = async (event) => {
  const { name, size, keys } = JSON.parse(event.body);

  try {
    console.log('Creating instance:', { name, size, keys });

    const createParams = {
      ImageId: 'ami-03eccfb168379b47e', // Replace with the desired AMI ID
      InstanceType: size,
      KeyName: keys,
      MinCount: 1, // Provide the minimum number of instances to launch
      MaxCount: 1, // Provide the maximum number of instances to launch
    };

    const data = await ec2.runInstances(createParams).promise();
    const instanceId = data.Instances[0].InstanceId;

    const tagParams = {
      Resources: [instanceId],
      Tags: [
        { Key: 'Name', Value: name },
      ],
    };

    await ec2.createTags(tagParams).promise();

    console.log('Instance created successfully');

    return {
      statusCode: 200,
      body: JSON.stringify({ message: 'Instance created successfully' }),
    };
  } catch (error) {
    console.error('Error creating instance:', error);

    return {
      statusCode: 500,
      body: JSON.stringify({ error: 'An error occurred while creating the instance' }),
    };
  }
}; 
